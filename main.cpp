#include "src/include/Importer.hpp"

#include <iostream>
#include <string>

using namespace std;

int main(){
    
    Importer imp;
    
	Graphe g = imp.import_graphe("datas.csv");

	vector<vector<int>> adjacente = g.getMatriceAdjacence();

    for (int i = 0; i < adjacente.size(); i++) {

        cout << "[";

        for (int e = 0; e < adjacente.at(i).size(); e++) {

            cout << adjacente.at(i).at(e);
            if (e < adjacente.at(i).size() - 1)
                cout << ";";

        }

        cout << "]" << endl;

    }

    cout << endl << "Parcours en profondeur a partir du point 2 " << endl;

    vector<int> marques;
    vector<int> *resultProfondeur = g.parcoursProfondeur(2, &marques);

    for (int i = 0; i < resultProfondeur->size(); i++) {

        cout << resultProfondeur->at(i) << " ; ";

    }

    cout << endl;

    cout << endl << "Parcours en largeur a partir du point 3 " << endl;

    vector<int> *resultLargeur = g.parcoursLargeur(3);

    for (int i = 0; i < resultLargeur->size(); i++) {

        cout << resultLargeur->at(i) << " ; ";

    }

    cout << endl;

    cout << endl << "Successeurs/Voisins du point 0 " << endl;

    vector<int> successeurs = g.getSuccesseurs(0);

    for (int i = 0; i < successeurs.size(); i++) {

        cout << successeurs.at(i) << " ; ";

    }

	cout << endl;

    cout << endl << "Composantes connexe " << endl;

    vector<vector<int>> composantes = g.composanteFortementConnexe();

    for (int i = 0; i < composantes.size(); i++) {

        cout << "Composante " << i << " : ";

        for (int e = 0; e < composantes.at(i).size(); e++) {

            cout << composantes.at(i).at(e) << " ; ";

        }

        cout << endl;

    }

    cout << endl << "Algorithme de Dijkstra" << endl;

    cout << "Distance min entre 0 et 3 : " << g.dijkstra(0, 3) << endl;
    cout << "Distance min entre 2 et 0 : " << g.dijkstra(2, 0) << endl;

    cout << endl << "Algorithme de Prim" << endl;

    Graphe result_prim;
    int sum_prim;
    g.prim(result_prim, sum_prim);
    cout << "Somme minimale de Prim : " << sum_prim << endl;
	
	cout << endl << "Algorithme de Kruskal" << endl;

	Graphe result_kruskal;
	int sum_kruskal;
	g.kruskal(result_kruskal, sum_kruskal);
	cout << "Somme minimale de Kruskal : " << sum_kruskal << endl;

	imp.export_graphe("../datas.csv",g);

    system("pause");

}
