#pragma once

#include <algorithm>
#include <map>
#include <vector>

class Graphe {

    private:

        //Attributs
        std::vector<std::vector<int>> matrice;
        std::map<std::pair<int,int>,int> cost_arcs;
        bool oriented;

        //Méthode(s) privée(s)
        std::vector<std::vector<int>> produitMatriciel(std::vector<std::vector<int>> vA, std::vector<std::vector<int>> vB);


    public:
        //Constructeurs
        Graphe(int n_sommets = 0, bool oriented = true);
        Graphe(std::vector<std::vector<int>> matrice, bool oriented);
            
        //Getters, setters
        int getCostArc(int row, int column);
        std::vector<int> getSuccesseurs(int sommet);
        std::vector<std::vector<int>> getMatriceAdjacence();
        std::vector<std::vector<int>> getMatriceIncidence();
        bool isOriented() const;

        //Méthodes
        void ajoutSommet();
        void ajoutSommet(std::vector<int> arcs);
        void ajoutArc(int som, int som2, int value_arc);

        std::vector<int> *parcoursProfondeur(int sommet, std::vector<int> *marques);
        std::vector<int> *parcoursLargeur(int sommet);

        std::vector<std::vector<int>> composanteFortementConnexe();

        int dijkstra(int origine, int destination);

        void prim(Graphe& g, int& sum_minimale);

        void kruskal(Graphe& g, int& sum_minimale);
};
