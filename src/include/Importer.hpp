#pragma once
#include <vector>
#include <string>
#include <fstream>

#include "Graphe.hpp"

class Importer{
    private : 
        std::string path;
        std::ifstream * file;


    public:
        //constructor
        Importer();
        Importer(std::string p);
        ~Importer();

        //getters, setters
        std::string get_path() const;
        void set_path(std::string);

        std::ifstream& get_file() const;

        //functions
        Graphe import_graphe(std::string url);
        void export_graphe(std::string url, Graphe g);
        static std::vector<std::string> split(std::string, char delimiter);


};
