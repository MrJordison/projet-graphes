#include "include/Graphe.hpp"

#include <iostream>

using namespace std;

Graphe::Graphe(int n_sommets, bool oriented){
    this->oriented = oriented;
    for (int i = 0; i < n_sommets;++i)
        matrice.push_back(vector<int>(n_sommets, 0));
}

Graphe::Graphe(vector<vector<int>> matrice, bool oriented) {
	
    this->matrice = matrice;
    this->oriented = oriented;

}

vector<vector<int>> Graphe::getMatriceAdjacence() {
    return matrice;
}

int Graphe::getCostArc(int row, int column) {
    if (!matrice.at(row).at(column))
        throw "L'arc recherch� n'existe pas dans le graphe";
    int res;
    try{
        res = cost_arcs.at(pair<int,int>(row,column));
    }
    catch(exception& e){
        res = cost_arcs.at(pair<int,int>(column,row)); 
    }
    return res;
}

vector<int> Graphe::getSuccesseurs(int sommet) {

    vector<int> successeurs;

    //On parcourt la ligne du sommet
    for (int i = 0; i < matrice.at(sommet).size(); i++) {
        //Si le sommet en cours est un successeur on l'ajoute
        if (matrice.at(sommet).at(i) == 1) {
            successeurs.push_back(i);
        }
    }

    return successeurs;
}

vector<vector<int>> Graphe::getMatriceIncidence(){
    vector<vector<int>> incidence;
    for(int i = 0; i < matrice.size(); ++i)
        incidence.push_back(vector<int>(cost_arcs.size(),0));

    int i = 0;
    for(map<pair<int,int>,int>::iterator it = cost_arcs.begin(); it!= cost_arcs.end(); ++it){
        incidence.at(it->first.first).at(i) = 1;
        if(!oriented)
            incidence.at(it->first.second).at(i) = 1;
        else 
            incidence.at(it->first.second).at(i) = -1;
        ++i;
    }

    return incidence;
}


void Graphe::ajoutSommet() {

    matrice.push_back(vector<int>(matrice.size(),0));

    //On ajoute un arc � 0 pour chaque sommet
    for (int i = 0; i < matrice.size(); i++)
        matrice.at(i).push_back(0);
}

void Graphe::ajoutSommet(vector<int> arcs) {

    if(arcs.size()!= matrice.size()+1)
        throw ("Erreur de taille pour l'ajout du sommet, nombre d'arcs possible sp�cifi� incorrect");
    else{
	//Pour chaque sommet du graphe on ajoute une nouvelle valeur d'arc
	for (int i = 0; i < matrice.size(); i++) {
            if(arcs.at(i)==1){
                cost_arcs.at(pair<int,int>(matrice.size(),i)) = 0;
                if(!oriented){
                    matrice.at(i).push_back(1);
                    cost_arcs.at(pair<int,int>(i,matrice.size())) = 0;
                }
            }
            if(arcs.at(i) == 0)
                matrice.at(i).push_back(0);
	}
	//On ajoute ensuite les nouveaux arcs
	matrice.push_back(arcs);
    }

}

vector<int> *Graphe::parcoursProfondeur(int sommet, vector<int> *marques) {

    //On marque le sommmet
    marques->push_back(sommet);

    //On r�cup�re la colonne de la matrice correspondant au sommet
    vector<int> successeur = matrice.at(sommet);

    for (int i = 0; i < successeur.size(); i++) {
        //Si il s'agit d'un successeur qui n'est pas marqu� dans la liste
        if (successeur.at(i) == 1 && find(marques->begin(), marques->end(), i) == marques->end()) {
            parcoursProfondeur(i, marques);
        }
    }

    return marques;
}

vector<int> *Graphe::parcoursLargeur(int sommet) {

    vector<int> *marques = new vector<int>();

    //On ajoute le sommet � la file
    vector<int> file;
    file.push_back(sommet);

    //Tant que l'on a des sommets dans la file
    while (file.size() != 0) {

        //On r�cup�re le premier �l�ment dans la file et on l'enl�ve de la file
        int s = file.at(0);
        file.erase(file.begin());

        //Si l'�l�ment n'est pas marqu�
        if (find(marques->begin(), marques->end(), s) == marques->end()) {
            //On le marque
            marques->push_back(s);

            vector<int> successeur = matrice.at(s);
            
            //Et on r�cup�re ses successeurs
            for (int i = 0; i < successeur.size(); i++) {
                if (successeur.at(i) == 1)
                    file.push_back(i);
            }
        }
    }

    return marques;
}

vector<vector<int>> Graphe::produitMatriciel(vector<vector<int>> vA, vector<vector<int>> vB) {

    //On cr�e notre matrice finale
    vector<vector<int>> produit;

    for (int i = 0; i < vA.size(); i++) {

        vector<int> column;
        for (int e = 0; e < vA.size(); e++) {
            int total = 0;
            for (int j = 0; j < vA.size(); j++) {
                total += (vA.at(e).at(j) * vB.at(j).at(e));
            }
            if (total > 0)
                total = 1;
            column.push_back(total);
        }

        produit.push_back(column);
    }

    return produit;
}

vector<vector<int>> Graphe::composanteFortementConnexe() {

    vector<vector<int>> adjacence = matrice;
	
    vector<vector<int>> matricePuissance = adjacence;
    vector<vector<int>> puissance = adjacence;
    vector<vector<int>> puissance2;

    for (int i = 0; i < puissance.size(); i++) {
        vector<int> tmp;
            
        for (int e = 0; e < puissance.size(); e++) {
                tmp.push_back(0);
        }
        puissance2.push_back(tmp);
    }

    //On r�cup�re ensuite la matrice contenant tout les chemins
    while (!equal(puissance.begin(), puissance.end(), puissance2.begin())) {

        puissance2 = puissance;
        puissance = produitMatriciel(puissance, adjacence);
        
        for (int i = 0; i < matricePuissance.size(); i++) {

            for (int e = 0; e < matricePuissance.at(i).size(); e++) {
                matricePuissance.at(i).at(e) += puissance.at(i).at(e);
                if (matricePuissance.at(i).at(e) > 1)
                    matricePuissance.at(i).at(e) = 1;
            }

        }

    }

    vector<vector<int>> composante;
    vector<vector<int>> structure;

    //On cherche ensuite les diff�rentes composantes connexe
    for (int i = 0; i < matricePuissance.size(); i++) {

        vector<int> structSommet;

        for (int e = 0; e < matricePuissance.at(i).size(); e++) {
            structSommet.push_back(matricePuissance.at(e).at(i));
        }

        bool trouve = false;
        int indiceComposante = 0;

        //On parcourt les diff�rentes colonnes que l'on a retenu
        for (int e = 0; e < structure.size(); e++) {
            if (equal(structure.at(e).begin(), structure.at(e).end(), structSommet.begin())) {
                indiceComposante = e;
                trouve = true;
            }
        }

        //Si aucune n'a �t� trouv� on l'ajoute
        if (!trouve) {
            vector<int> tmp;
            tmp.push_back(i);
            composante.push_back(tmp);
            structure.push_back(structSommet);
        }
        else { //Sinon on ajoute le sommet dans la composante
            composante.at(indiceComposante).push_back(i);
        }

    }

    //On retourne les composantes
    return composante;

}

void Graphe::kruskal(Graphe& g, int& sum_minimale){

    //initialisation de l'arbre couvrant minimale et de la somme des arcs de cet arbre
    g = Graphe(matrice.size(), oriented);
    sum_minimale = 0;

    //cr�ation vecteur contenant les arcs tri�es par cout croissant
    vector<pair<int,int>> ordering = vector<pair<int,int>>();
    ordering.push_back(cost_arcs.begin()->first);
    map<pair<int,int>,int>::iterator it;
    unsigned int i;
    //ajout de chaque arc dans la liste tri�e
    for(it = ++cost_arcs.begin(); it!=cost_arcs.end(); ++it){
        i=0;
        while(i < ordering.size() && (cost_arcs.at(ordering.at(i)) < it->second)){
            ++i;
        }
        ordering.insert(ordering.begin()+i, it->first);
    }
    
    //cr�ation d'un vecteur contenant une composante pour chaque sommet du graphe
    vector<vector<int>> composantes;
    for(i = 0; i <matrice.size(); ++i)
        composantes.push_back(vector<int>(1,i));

    i = 0;
    int extinit, extfinal;
    unsigned int j;

    //it�ration tant que tous les arcs n'ont pas �t� parcourus ou que le nombre de sous composantes est sup�rieur � 1
    while(i < ordering.size() && composantes.size()!=1){

        extinit = extfinal = -1;
        j = 0;

        //recherche des sous composantes contenant les extr�mit�s de l'arc actuellement trait�
        while((j < composantes.size()) && (extinit == -1 || extfinal == -1)){
            if(extinit == -1)
                extinit = find(composantes.at(j).begin(),composantes.at(j).end(),ordering.at(i).first)!=composantes.at(j).end() ? j : -1;
            if(extfinal == -1)
                extfinal = find(composantes.at(j).begin(),composantes.at(j).end(),ordering.at(i).second)!=composantes.at(j).end() ? j : -1;

            ++j;
        }

        //si les deux extr�mit�s de l'arc ne sont pas dans la m�me sous composante, on ajoute l'arc
        if(extinit!=extfinal){

            g.ajoutArc(ordering.at(i).first,ordering.at(i).second, cost_arcs.at(ordering.at(i)));

            //ajout du co�t de l'arc � la somme minimale
            sum_minimale += cost_arcs.at(ordering.at(i));

            //fusion des deux sous composantes en une seule et supprime la seconde
            composantes.at(extinit).insert(composantes.at(extinit).end(),composantes.at(extfinal).begin(),composantes.at(extfinal).end());
            composantes.erase(composantes.begin()+extfinal);
           
        }

        ++i;
    }
}

void Graphe::ajoutArc(int som, int som2, int cost){
   
    matrice.at(som).at(som2) = 1;
    if(!oriented)
        matrice.at(som2).at(som) = 1;

    cost_arcs.insert(pair<pair<int,int>,int>(pair<int,int>(som,som2),cost));

}

bool Graphe::isOriented() const{
    return oriented;
}

int Graphe::dijkstra(int origine, int destination) {

    map<int, int> distances; //Contient les distances entre l'origine et les sommets (premier : sommets, second : co�t)
    vector<int> aTraiter;
    
    distances.insert(distances.begin(), pair<int, int>(origine, 0));

    vector<int> premiersVoisin = getSuccesseurs(origine);

    //On initialise les valeurs de distances
    for (int i = 0; i < matrice.size(); i++) {
        if (i != origine) {
            //Si le sommet en cours n'est pas un voisin de l'origine on initialise le co�t � -1
            if(find(premiersVoisin.begin(), premiersVoisin.end(), i) == premiersVoisin.end())
                distances.insert(distances.begin(), pair<int, int>(i, -1));
            else { //Sinon on r�cup�re le co�t
                distances.insert(distances.begin(), pair<int, int>(i, getCostArc(origine, i)));
            }
            aTraiter.push_back(i);
        }

    }

    //Tant que l'on a des sommets � traiter
    while (aTraiter.size() > 0) {

        int e = 0;
        int sEnCours = -1;
        int dMin = -1;

        //On cherche le sommet le plus proche (celui qui a le dmin le plus bas)
        for (int i = 0; i < aTraiter.size(); i++) {
            if (distances.at(aTraiter.at(i)) != -1 && (sEnCours == -1 || distances.at(aTraiter.at(i)) < dMin)) {
                sEnCours = aTraiter.at(i);
                dMin = distances.at(sEnCours);
            }
        }

        //Si on a trouv� un autre sommet dans la liste
        if (sEnCours > -1) {

            //On enl�ve le sommet de ceux a traiter
            aTraiter.erase(find(aTraiter.begin(), aTraiter.end(), sEnCours));

            vector<int> success = getSuccesseurs(sEnCours);

            //Si le sommet en cours a des successeurs
            if (success.size() > 0) {
                //Pour chaque voisin du sommet en cours on met � jour la distance entre l'origine et ce sommet
                for (int i = 0; i < success.size(); i++) {

                    if (find(aTraiter.begin(), aTraiter.end(), success.at(i)) != aTraiter.end()) {
                        int ndist = distances.at(sEnCours) + getCostArc(sEnCours, success.at(i));
                        if(distances.at(success.at(i)) == -1 || ndist < distances.at(success.at(i)))
                            distances.at(success.at(i)) = ndist;
                    }

                }

            }

        }

    }

    return distances.at(destination);

}

void Graphe::prim(Graphe& g, int& sum_minimale){
    int i;

    //initialisation du tableau des co�ts minimum pour chaque sommet du graphe � construire
    int** dist = new int*[matrice.size()];
    bool* ajoute = new bool[matrice.size()];
    for(i = 0; i < matrice.size(); ++i){
        ajoute[i] = false; 
        dist[i] = nullptr;
    }
    g = Graphe(matrice.size());
    sum_minimale = 0;

    //stockage du premier sommet "ajout�" au graphe minimal
    int actual_sommet = 0;
    ajoute[0] = true;

    int count = 1;
    int to_add;
    vector<int> success;

    //boucle tant que tous les sommets ne seront pas ajout�s au graphe couvrant minimal
    while(count < matrice.size()){
        //r�cup�re les successeurs du dernier sommet ajout� au graphe minimal
        success = getSuccesseurs(actual_sommet);
        
        //pour chacun des successeurs
        for(i = 0; i < success.size(); ++i){

            //regarde si la valeur minimale pour atteindre le successeur est plus petite que celle d�j� trouv�e
            //si c'est le cas ou si le successeur n'a pas encore �t� trouv�, on met � jour le distleau des distances 
            if(!ajoute[success.at(i)]){
                if(dist[success.at(i)] == nullptr){
                    dist[success.at(i)] = new int(getCostArc(actual_sommet,success.at(i)));
                }
                else if(getCostArc(actual_sommet, success.at(i)) < *(dist[success.at(i)])){
                    *(dist[success.at(i)]) = getCostArc(actual_sommet,success.at(i));
                }
            }
        }
        to_add = -1;
        
        //recherche de la distance la plus petite dans le tableau des distance
        for(i = 0; i < matrice.size(); ++i){
            if(dist[i] != nullptr){
                if(!ajoute[i] && to_add==-1)
                    to_add = i;
                else if (*dist[i] < *dist[to_add])
                    to_add = i;
            }
        }

        //ajout du sommet marqu� au graphe minimal avec l'arc correspondant, addition de la distance avec la somme minimale � retourner
        g.ajoutArc(actual_sommet, to_add, *dist[to_add]);
        sum_minimale += *dist[to_add];
        actual_sommet = to_add;
        ajoute[to_add] = true;
        count++;
        delete dist[to_add];
        dist[to_add] = nullptr;
    }
}

