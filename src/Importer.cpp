#include <iostream>
#include <sstream>
#include "include/Importer.hpp"

using namespace std;

//Constructeurs
Importer::Importer(){
    path="";
    file = nullptr;
}

Importer::Importer(string p){
    set_path(p);
}

Importer::~Importer(){
    if(file)
        delete file;
    file = nullptr;
}

//Méthodes
string Importer::get_path() const{
    return path;
}

void Importer::set_path(string p){
    path = p;
    file = new ifstream(path, ios::in);
    if(file) 
        cout<< "ouverture fichier"<<endl;
    else
        cout<<" problème ouverture"<<endl;
}

ifstream& Importer::get_file() const{
    return *file;
}

Graphe Importer::import_graphe(string url){
    set_path(url);
    vector<vector<int>> matrice;
	bool oriented = false;

    string contenu("");
    int cpt=0;
    vector<string> line_split;
	map<pair<int, int>, int> cost_arcs;
    
    //Test si le fichier existe ou non
    if(file){
        //Parcours de toutes les lignes du fichier définissant le graphe
        while(getline(*file,contenu)){
            ++cpt;
            switch(cpt){ 
                //1e ligne : dimensions matrice carré
                case 1:
                    //génération d'une matrice vide
                    for(int i(0); i<stoi(contenu); ++i){
                        matrice.push_back(vector<int>());
                        for(int j(0); j<stoi(contenu);++j)
                            matrice.at(i).push_back(0);
                    }
                    break;
                
                //2e ligne : indique si le graphe est orientée (1) ou non orientée (0)
                case 2:
                    if (stoi(contenu) == 1)
                        oriented = true;
                    break;

                default:
                    //Au delà de la 2e ligne : contient les valeurs des arcs du graphe
                    if(cpt>2 && contenu!=""){
                        line_split = split(contenu,';');
                        matrice.at(stoi(line_split[0])).at(stoi(line_split[1])) = 1;
                        cost_arcs.insert(pair<pair<int, int>, int>(pair<int, int>(stoi(line_split[0]), stoi(line_split[1])), stoi(line_split[2])));
                    }
                    break;
            }
        }
    }

    Graphe res(matrice,oriented);

    for (map<pair<int, int>, int>::iterator it = cost_arcs.begin(); it != cost_arcs.end(); ++it) {
        int s1 = (*it).first.first;
        int s2 = (*it).first.second;
        int cout = (*it).second;
        res.ajoutArc(s1, s2, cout);
    }

    return res;
} 

void Importer::export_graphe(string url, Graphe g){
    int size = g.getMatriceAdjacence().size();
    string res = to_string(size)+"\n";
	
    if (g.isOriented())
        res += "1\n";
    else
        res += "0\n";

    for(int i(0); i<size; ++i){
        for(int j(0);j<size; ++j){
            if(g.getMatriceAdjacence().at(i).at(j) != 0){
                res += to_string(i) + ";" + to_string(j);
                res += ";"+to_string(g.getCostArc(i, j));
                res += "\n";
            }
        }
    }

    ofstream fichier (url, ios::out | ios::trunc);
    if(fichier){
        fichier << res << endl;
        fichier.close();
    }
    else
        throw "Erreur création fichier";
}

//Fonction permettant de split un string d'après un char
vector<string> Importer::split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;
    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }               
    return internal;
}
